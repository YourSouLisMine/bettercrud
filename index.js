const express = require('express');
const bodyParser = require('body-parser');
const handler = require('./routes/handler');
const mongoose = require('mongoose');
const app = express();

mongoose.connect('mongodb+srv://ysim:ABC123@cluster0-ot8x0.mongodb.net/test');

var db = mongoose.connection;

app.use(bodyParser.json());

app.use(function(req, res, next) {
    let globalRes = res;
    process.on('unhandledRejection', function(reason, p) {
        console.log(reason);
        try {
            return globalRes.send({
                status: false,
                data: {
                    err: `${reason.type}`,
                    userMsg: 'Error interno.'
                }
            }).sendStatus(500);
        } catch (err) {
            return 0;
        }
    });

    next();
});

// app.use(express.static('public'));

db.once('open', function() {
    app.listen(process.env.PORT || 4000, () => {
        console.log('Now listening for');
    });
});


db.on('error', function(err) {
    console.log(err);
});

app.use('/', handler);