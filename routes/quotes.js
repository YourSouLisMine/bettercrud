const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Promise = require('bluebird')
const Quotes = require('.././models/quotes');
const helper = require('./helpers/index');
const helperQuotes = require('./helpers/quotes');
const validator = require('validator');

router.get('/quotes', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helper.getQuery(req.query, helperQuotes.regexJson.search).then(function(query) {
            helperQuotes.getQuotes(query).then(function(quotes) {
                dataServer.data = quotes;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.get('/quotes/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperQuotes.getQuotesById(req.params).then(function(quotes) {
                dataServer.data = quotes;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            })
            .catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            })
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.get('/sumMoney', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperQuotes.sumMoney(req.query).then(function(modifiedReq){
            helper.aggregateBuilder(modifiedReq).then(function(quotes) {
                Quotes.aggregate(quotes).then(function(agg){
                    if (agg.length) {
                        dataServer.data = agg;
                        helper.response(dataServer).then(function(data) {
                            return res.send(data);
                        });
                    } else {
                        dataServer.data = false;
                        helper.response(dataServer).catch(function(err) {
                            return res.send(err);
                        });
                    }
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        });
    });
});

//agregar
router.get('/sumMoneyPrices', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperQuotes.sumMoneyPrices(req.query).then(function(modifiedReq){
            helper.aggregateBuilder(modifiedReq).then(function(quotes) {
                Quotes.aggregate(quotes).then(function(agg){
                    if (agg.length) {
                        dataServer.data = agg;
                        helper.response(dataServer).then(function(data) {
                            return res.send(data);
                        });
                    } else {
                        dataServer.data = false;
                        helper.response(dataServer).catch(function(err) {
                            return res.send(err);
                        });
                    }
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        });
    });
});

router.post('/quotes', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperQuotes.postQuotes(req.body).then(function(quotes) {
            dataServer.data = quotes;
            helper.response(dataServer).then(function(data) {
                return res.send(data);
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.put('/quotes/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperQuotes.putQuotes(req.params, req.body).then(function(quotes) {
                dataServer.data = quotes;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.delete('/quotes/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperQuotes.deleteQuotes(req.params).then(function(quotes) {
                dataServer.data = quotes;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            })
            .catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            })
    });
});

module.exports = router;