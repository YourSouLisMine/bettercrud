const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Promise = require('bluebird')
const Service = require('.././models/service');
const helper = require('./helpers/index');
const helperService = require('./helpers/service');
const validator = require('validator');

router.get('/service', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helper.getQuery(req.query, helperService.regexJson.search).then(function(query) {
            helperService.getService(query).then(function(service) {
                dataServer.data = service;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.get('/service/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperService.getServiceById(req.params).then(function(service) {
                dataServer.data = service;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.post('/service', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperService.postService(req.body).then(function(service) {
            dataServer.data = service;
            helper.response(dataServer).then(function(data) {
                return res.send(data);
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.put('/service/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperService.putService(req.params, req.body).then(function(service) {
                dataServer.data = service;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.delete('/service/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperService.deleteService(req.params).then(function(service) {
                dataServer.data = service;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

module.exports = router;