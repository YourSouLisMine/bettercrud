const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Promise = require('bluebird')
const User = require('.././models/user');
const helper = require('./helpers/index');
const helperUser = require('./helpers/user');
const helperBill = require('./helpers/bill');
const validator = require('validator');
const fs = require('fs');

router.get('/test', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperBill.bill()
    });
});

router.get('/user', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helper.getQuery(req.query, helperUser.regexJson.search).then(function(query) {
            helperUser.getUser(query).then(function(user) {
                dataServer.data = user;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.get('/user/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperUser.getUserById(req.params).then(function(user) {
                dataServer.data = user;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.post('/user', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperUser.uploadFile(req).then(function(pathAndFields) {
            helperUser.imageResize(pathAndFields[0]).then(function(paths) {
                helperUser.addPath(paths, pathAndFields[1]).then(function(object) {
                    helperUser.postUser(object).then(function(user) {
                        dataServer.data = user;
                        helper.response(dataServer).then(function(data) {
                            return res.send(data);
                        });
                    }).catch(function(errorString) {
                        dataServer.data = errorString;
                        helper.response(dataServer).catch(function(err) {
                            return res.send(err);
                        });
                    });
                }).catch(function(errorString) {
                    dataServer.data = errorString;
                    helper.response(dataServer).catch(function(err) {
                        return res.send(err);
                    });
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        });
    });
});

router.put('/user/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperUser.modifyFile(req, req.params).then(function(pathAndFields) {
                helperUser.imageResize(pathAndFields[0]).then(function(paths) {
                    helperUser.addPath(paths, pathAndFields[1]).then(function(object) {
                        helperUser.putUser(req.params, object).then(function(user) {
                            dataServer.data = user;
                            helper.response(dataServer).then(function(data) {
                                return res.send(data);
                            });
                        }).catch(function(errorString) {
                            fs.rmdir(object.photo[0].substring(0, 48));
                            dataServer.data = errorString;
                            helper.response(dataServer).catch(function(err) {
                                return res.send(err);
                            });
                        });
                    }).catch(function(errorString) {
                        dataServer.data = errorString;
                        helper.response(dataServer).catch(function(err) {
                            return res.send(err);
                        });
                    });
                }).catch(function(errorString) {
                    dataServer.data = errorString;
                    helper.response(dataServer).catch(function(err) {
                        return res.send(err);
                    });
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.delete('/user/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperUser.deleteUser(req.params).then(function(user) {
                dataServer.data = user;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

module.exports = router;