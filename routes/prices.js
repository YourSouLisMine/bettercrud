const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Promise = require('bluebird');
const Prices = require('.././models/prices');
const helper = require('./helpers/index');
const helperPrices = require('./helpers/prices');
const validator = require('validator');

router.get('/prices', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helper.getQuery(req.query, helperPrices.regexJson.search).then(function(query) {
            helperPrices.getPrices(query).then(function(prices) {
                dataServer.data = prices;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.get('/prices/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperPrices.getPrizeById(req.params).then(function(prices) {
                dataServer.data = prices;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.get('/sumPrices', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperPrices.sumPrices(req.query).then(function(modifiedReq) {
            helper.aggregateBuilder(modifiedReq).then(function(prices) {
                Prices.aggregate(prices).then(function(agg) {
                    if (agg.length) {
                        dataServer.data = agg;
                        helper.response(dataServer).then(function(data) {
                            return res.send(data);
                        });
                    } else {
                        dataServer.data = false;
                        helper.response(dataServer).catch(function(err) {
                            return res.send(err);
                        });
                    }
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        });
    });
});

router.post('/prices', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperPrices.postPrize(req.body).then(function(prices) {
            dataServer.data = prices;
            helper.response(dataServer).then(function(data) {
                return res.send(data);
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.put('/prices/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperPrices.putPrize(req.params, req.body).then(function(prices) {
                dataServer.data = prices;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.delete('/prices/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperPrices.deletePrize(req.params).then(function(prices) {
                dataServer.data = prices;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

module.exports = router;