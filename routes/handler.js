const express = require('express');
const handler = express();

let userService = require('./userService');
let service = require('./service');
let user = require('./user');

handler.use('/api', userService);
handler.use('/api', service);
handler.use('/api', user);


module.exports = handler;