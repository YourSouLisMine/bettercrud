const Promise = require('bluebird');
const Quotes = require('../.././models/quotes');
const mongoose = require('mongoose');

module.exports = {
    getQuotes: function(query) {
        return new Promise(function(resolve, reject) {
            Quotes.find(query).then(function(quotes) {
                if (quotes.length) {
                    return resolve(quotes);
                } else {
                    return reject(false);
                }
            })
        });
    },

    getQuotesById: function(params) {
        return new Promise(function(resolve, reject) {
            Quotes.findOne(params).then(function(quotes) {
                if (quotes != undefined) {
                    return resolve(quotes);
                } else {
                    return reject(false);
                }
            });
        });
    },

    postQuotes: function(body) {
        return new Promise(function(resolve, reject) {
            Quotes.create(body).then(function(quotes) {
                return resolve(quotes);
            }).catch(function() {
                return reject(false);

            });
        });
    },

    putQuotes: function(params, body) {
        return new Promise(function(resolve, reject) {
            Quotes.findOneAndUpdate(params, body, {
                new: true
            }).then(function(quotes) {
                if (quotes != undefined) {
                    return resolve(quotes);
                } else {
                    return reject(false);
                }
            });

        });
    },

    deleteQuotes: function(params) {
        return new Promise(function(resolve, reject) {
            Quotes.findOne(params).then(function(quotes) {
                if (quotes != undefined) {
                    Quotes.remove({
                        _id: quotes._id
                    }).then(function() {
                        return resolve(quotes);
                    });
                } else {
                    return reject(false);
                }
            }).catch(function() {
                return reject(false);
            });
        });
    },

    sumMoney: function(req) {
        let app = this;
        return new Promise(function(resolve, reject) {
            let aggJson = app.aggJson;
            delete aggJson.lookup;
            delete aggJson.addFields;
            if (req._id) {
                let ids = req._id.split(',')
                let i = 0;
                for (i; i < ids.length; i++) {
                    ids[i] = mongoose.Types.ObjectId(ids[i]);
                }
                req._id = {
                    "$in": ids
                };
            }
            if (req.money) {
                req.money = Number(req.money);
            }
            aggJson.match.params = req;
            return resolve(aggJson);
        });
    },

    sumMoneyPrices: function(req) {
        let app = this;
        return new Promise(function(resolve, reject) {
            let aggJson = app.aggJson;
            if (req._id) {
                let ids = req._id.split(',')
                let i = 0;
                for (i; i < ids.length; i++) {
                    ids[i] = mongoose.Types.ObjectId(ids[i]);
                }
                req._id = {
                    "$in": ids
                };
            }
            if (req.money) {
                req.money = Number(req.money);
            }
            aggJson.match.params = req;
            return resolve(aggJson);
        });
    },

    regexJson: {
        "search": /\bname\b|\bmoney\b|\bquote\b/g
    },

    aggJson: {
        "project": {
            "rules": ["__v"],
            "show": false
        },
        "match": {
            "rules": /\b_id\b|\bname\b|\bmoney\b|\bquote\b/g,
            "require": ["name"],
            "params": null //req.body, req.query
        },
        "group": {
            "rules": ["_id", "total"],
            "actions": ["$name", {
                $sum: "$money"
            }]
        },
        "lookup": {
            "rules": ["from", "localField", "foreignField", "as"],
            "actions": ["prices", "_id", "player", "pricesCollection"]
        },
        "addFields": {
            "rules": ["totalAll"],
            "actions": [{
                $sum: ["$total", {
                    $sum: "$pricesCollection.prices"
                }]
            }]
        }
    }
}