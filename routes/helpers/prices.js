const Promise = require('bluebird');
const Prices = require('../.././models/prices');
const mongoose = require('mongoose');

module.exports = {
    getPrices: function(query) {
        return new Promise(function(resolve, reject) {
            Prices.find(query).then(function(prices) {
                if (prices.length) {
                    return resolve(prices);
                } else {
                    return reject(false);
                }
            });
        });
    },

    getPrizeById: function(params) {
        return new Promise(function(resolve, reject) {
            Prices.findOne(params).then(function(prices) {
                if (prices != undefined) {
                    return resolve(prices);
                } else {
                    return reject(false);
                }
            });
        });
    },

    postPrize: function(body) {
        return new Promise(function(resolve, reject) {
            Prices.create(body).then(function(prices) {
                return resolve(prices);
            }).catch(function() {
                return reject(false);

            });
        });
    },

    putPrize: function(params, body) {
        return new Promise(function(resolve, reject) {
            Prices.findOneAndUpdate(params, body, {new: true}).then(function(prices) {
                if (prices != undefined) {
                    return resolve(prices);
                } else {
                    return reject(false);
                }
            });

        });
    },

    deletePrize: function(params) {
        return new Promise(function(resolve, reject) {
            Prices.findOne(params).then(function(prices) {
                if (prices != undefined) {
                    Prices.remove({
                        _id: prices._id
                    }).then(function() {
                        return resolve(prices);
                    });
                } else {
                    return reject(false);
                }
            }).catch(function() {
                return reject(false);
            });
        });
    },

    sumPrices: function(req) {
        let app = this;
        return new Promise(function(resolve, reject) {
            let aggJson = app.aggJson;
            if (req._id) {
                let ids = req._id.split(',')
                let i = 0;
                for (i; i < ids.length; i++) {
                    ids[i] = mongoose.Types.ObjectId(ids[i]);
                }
                req._id = {
                    "$in": ids
                };
            }
            if(req.prices){
                req.prices = Number(req.prices);
            }
            aggJson.match.params = req;
            return resolve(aggJson);
        });
    },

    regexJson: {
        "search": /\bplayer\b|\bprices\b/g
    },

    aggJson: {
        "project": {
            "rules": ["__v"],
            "show": false
        },
        "match": {
            "rules": /\b_id\b|\bplayer\b|\bprices\b/g,
            "require": ["player"],
            "params": null //req.body, req.query
        },
        "group": {
            "rules": ["_id", "total"],
            "actions": ["$player", {$sum: "$prices"}]
        } 
    }
}