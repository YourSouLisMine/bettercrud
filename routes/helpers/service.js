const Promise = require('bluebird');
const Service = require('../.././models/service');
const mongoose = require('mongoose');

module.exports = {
    getService: function(query) {
        return new Promise(function(resolve, reject) {
            Service.find(query).then(function(service) {
                if (service.length) {
                    return resolve(service);
                } else {
                    return reject(false);
                }
            });
        });
    },

    getServiceById: function(params) {
        return new Promise(function(resolve, reject) {
            Service.findOne(params).then(function(service) {
                if (service != undefined) {
                    return resolve(service);
                } else {
                    return reject(false);
                }
            });
        });
    },

    postService: function(body) {
        return new Promise(function(resolve, reject) {
            Service.create(body).then(function(service) {
                return resolve(service);
            }).catch(function() {
                return reject(false);
            });
        });
    },

    putService: function(params, body) {
        return new Promise(function(resolve, reject) {
            Service.findOneAndUpdate(params, body, {
                new: true
            }).then(function(service) {
                if (service != undefined) {
                    return resolve(service);
                } else {
                    return reject(false);
                }
            });
        });
    },

    deleteService: function(params) {
        return new Promise(function(resolve, reject) {
            Service.findOne(params).then(function(service) {
                if (service != undefined) {
                    Service.remove({
                        _id: service._id
                    }).then(function() {
                        return resolve(service);
                    });
                } else {
                    return reject(false);
                }
            }).catch(function() {
                return reject(false);
            });
        });
    },

    regexJson: {
        "search": /\bname\b/g
    }
}