const Promise = require('bluebird');
const User = require('../.././models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const formidable = require('formidable');
const im = require('imagemagick-native-promise');
const fs = require('fs');

module.exports = {
    getUser: function(query) {
        return new Promise(function(resolve, reject) {
            User.find(query).then(function(user) {
                if (user.length) {
                    return resolve(user);
                } else {
                    return reject(false);
                }
            });
        });
    },

    getUserById: function(params) {
        return new Promise(function(resolve, reject) {
            User.findOne(params).then(function(user) {
                if (user != undefined) {
                    return resolve(user);
                } else {
                    return reject(false);
                }
            });
        });
    },

    postUser: function(body) {
        const saltRounds = 10;
        return new Promise(function(resolve, reject) {
            bcrypt.genSalt(saltRounds).then(function(salt) {
                bcrypt.hash(body.password, salt).then(function(hash) {
                    body.password = hash;
                    User.create(body).then(function(user) {
                        return resolve(user);
                    }).catch(function(err) {
                        fs.unlink(body.photo[0]);
                        fs.unlink(body.photo[1]);
                        fs.rmdir(body.photo[0].substring(0, 48));
                        return reject(false);
                    });
                });
            });
        });
    },

    putUser: function(params, body) {
        const saltRounds = 10;
        return new Promise(function(resolve, reject) {
            bcrypt.genSalt(saltRounds).then(function(salt) {
                bcrypt.hash(body.password, salt).then(function(hash) {
                    body.password = hash;
                    User.findOneAndUpdate(params, body, {
                        new: true
                    }).then(function(user) {
                        if (user != undefined) {
                            return resolve(user);
                        } else {
                            return reject(false);
                        }
                    });
                });
            });
        });
    },

    deleteUser: function(params) {
        return new Promise(function(resolve, reject) {
            User.findOne(params).then(function(user) {
                if (user != undefined) {
                    User.remove(params).then(function(test) {
                        fs.unlink(user.photo[0]);
                        fs.unlink(user.photo[1]);
                        fs.rmdir(user.photo[0].substring(0, 48));
                        return resolve(user);
                    });
                } else {
                    return reject(false);
                }
            }).catch(function() {
                return reject(false);
            });
        });
    },

    regexJson: {
        "search": /\bname\b|\bemail\b|\bnickname\b/g
    },

    idGenerator: function() {
        return new Promise(function(resolve, reject) {
            let id = mongoose.Types.ObjectId();
            let params = {
                "_id": id
            };
            User.findOne(params).then(function(user) {
                if (user == null) {
                    return resolve(id);
                } else {
                    return reject(false);
                }
            }).catch(function() {
                return reject(false);
            });
        });
    },

    uploadFile: function(req) {
        let app = this;
        return new Promise(function(resolve, reject) {
            let form = new formidable.IncomingForm();
            app.idGenerator().then(function(id) {
                let path = '././public/img/profiles/' + id;
                if (!fs.existsSync(path)) {
                    fs.mkdirSync(path);
                }
                form.uploadDir = path;
                form.on('file', function(field, file) {
                    fs.rename(file.path, path + '/fullResolution.jpg');
                });

                form.parse(req, function(files, fields) {
                    fields["_id"] = id;
                    let pathAndFields = [path, fields];
                    return resolve(pathAndFields);
                });
            }).catch(function() {
                return reject(false);
            });
        });
    },

    modifyFile: function(req, params) {
        let app = this;
        return new Promise(function(resolve, reject) {
            let form = new formidable.IncomingForm();
            let path = '././public/img/profiles/' + params._id;
            if (!fs.existsSync(path)) {
                return reject(false);
            }

            form.uploadDir = path;
            form.on('file', function(field, file) {
                fs.rename(file.path, path + '/fullResolution.jpg');
            });

            form.parse(req, function(files, fields) {
                let pathAndFields = [path, fields];
                return resolve(pathAndFields);
            });
        });
    },

    imageResize: function(path) {
        return new Promise(function(resolve, reject) {
            if (fs.existsSync(path)) {
                let pathFullRes = path + '/fullResolution.jpg';
                im.convert({
                        srcData: fs.readFileSync(pathFullRes),
                        width: 100,
                        height: 100,
                        resizeStyle: 'aspectfill', // is the default, or 'aspectfit' or 'fill'
                        gravity: 'Center' // optional: position crop area when using 'aspectfill'
                    })
                    .then(function(buffer) {
                        let pathMinRes = path + '/minResolution.jpg';
                        fs.writeFileSync(pathMinRes, buffer);
                        let paths = [pathFullRes, pathMinRes];
                        return resolve(paths);
                    })
                    .catch(function() {
                        return reject(false);
                    });
            } else {
                return reject(false);
            }
        });
    },

    // Bucket aws upload
    // awsUpload: function() {
    //     return new Promise(function(resolve, reject) {
    //         AWS.config.update({
    //             accessKeyId: "AKIAIN5N2OIGQF7NEFVQ",
    //             secretAccessKey: "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"
    //         });
    //         var s3 = new AWS.S3();
    //         var params = {
    //             Bucket: "test"
    //             // ,
    //             // Key: 'mykey.txt',
    //             // Body: "HelloWorld"
    //         };
    //         s3.createBucket(params, function(err, data) {
    //             if (err) console.log(err, err.stack);
    //             else console.log("Succesfully created... ");
    //         });

    //         // s3.putObject(params, function(err, res) {
    //         //     if (perr) {
    //         //         console.log("Error uploading data: ", err);
    //         //     } else {
    //         //         console.log("Successfully uploaded data to myBucket/myKey");
    //         //     }
    //         // });
    //     });
    // }

    addPath: function(path, user) {
        return new Promise(function(resolve, reject) {
            user["info"] = JSON.parse(user.info);
            user["photo"] = path;
            return resolve(user);
        });
    }


}