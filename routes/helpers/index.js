const Promise = require('bluebird')
    // const validator = require('validator');

module.exports = {
    response: function(data = false) {
        return new Promise(function(resolve, reject) {
            let type = data.data ? `${data.method} ${data.url}` : `ERROR ${data.method} ${data.url}`;
            if (data.data) {
                return resolve({
                    status: true,
                    type: type,
                    data: data.data
                });
            } else {
                return reject({
                    status: false,
                    type: type,
                    data: null
                });
            }
        });
    },

    getDataServer: function(req) {
        return new Promise(function(resolve, reject) {
            return resolve({
                method: req.method,
                url: req.originalUrl
            });
        });
    },

    getQuery: function(req, regex /*= /default/ */ ) {
        return new Promise(function(resolve, reject) {
            let params = Object.keys(req);
            if (params.length) {
                params = params.toString().match(regex);
                if (params) {
                    let data = {};
                    Promise.map(params, function(index) {
                        if (req[index] != undefined && req[index] != "" && req[index] != null) {
                            data[index] = req[index];
                        } else {
                            return reject(false);
                        }
                    }).then(function() {
                        return resolve(data);
                    });
                } else {
                    return reject(false);
                }
            } else {
                return resolve({});
            }
        });
    },

    // // let ids = query._id.split(",");
    // aggregateBuilder: function(aggJson) {
    //     let app = this;
    //     return new Promise(function(resolve, reject) {
    //         let keys = Object.keys(aggJson);
    //         let dataArray = [];
    //         Promise.map(keys, function(option) {
    //             return new Promise(function(success, failure) {
    //                 app.aggregateJson[option](aggJson[option], app).then(function(data) {
    //                     return success(data);
    //                 }).catch(function() {
    //                     return reject(false);
    //                 });
    //             });
    //         }).then(function(data) {
    //             return resolve(data);
    //         });

    //     });
    // },

    // aggregateJson: {
    //     "project": function aggregateProject(project) {
    //         return new Promise(function(resolve, reject) {
    //             let projectQuery = {
    //                 "$project": {}
    //             };
    //             let show = project.show ? 1 : 0;
    //             Promise.map(project.rules, function(option) {
    //                 projectQuery.$project[option] = show;
    //             }).then(function() {
    //                 return resolve(projectQuery);
    //             });
    //         });
    //     },

    //     "match": function aggregateMatch(match, app) {
    //         return new Promise(function(resolve, reject) {
    //             let matchQuery = {
    //                 "$match": {}
    //             };
    //             if (match.params[match.require]) {
    //                 app.getQuery(match.params, match.rules).then(function(option) {
    //                     matchQuery.$match = option;
    //                     return resolve(matchQuery);

    //                 });
    //             } else {
    //                 return reject(false);
    //             }

    //         });
    //     },

    //     "group": function aggregateGroup(group) {
    //         return new Promise(function(resolve, reject) {
    //             let groupQuery = {
    //                 "$group": {}
    //             };
    //             let i = 0
    //             for (i; i < group.rules.length && i < group.actions.length; i++) {
    //                 groupQuery.$group[group.rules[i]] = group.actions[i];
    //             }
    //             return resolve(groupQuery);
    //         });
    //     },

    //     "lookup": function aggregateLookUp(lookup) {
    //         return new Promise(function(resolve, reject) {
    //             let lookupQuery = {
    //                 "$lookup": {}
    //             };
    //             let i = 0
    //             for (i; i < lookup.rules.length && i < lookup.actions.length; i++) {
    //                 lookupQuery.$lookup[lookup.rules[i]] = lookup.actions[i];
    //             }
    //             return resolve(lookupQuery);
    //         });
    //     },

    //     "addFields": function aggregateAddFields(addFields) {
    //         return new Promise(function(resolve, reject) {
    //             let addFieldsQuery = {
    //                 "$addFields": {}
    //             };
    //             let i = 0
    //             for (i; i < addFields.rules.length && i < addFields.actions.length; i++) {
    //                 addFieldsQuery.$addFields[addFields.rules[i]] = addFields.actions[i];
    //             }
    //             return resolve(addFieldsQuery);
    //         });
    //     }
    // },


}