const Promise = require('bluebird');
const UserService = require('../.././models/userService');
const mongoose = require('mongoose');

module.exports = {
    getUserService: function(query) {
        return new Promise(function(resolve, reject) {
            UserService.find(query).then(function(userService) {
                if (userService.length) {
                    return resolve(userService);
                } else {
                    return reject(false);
                }
            });
        });
    },

    getUserServiceById: function(params) {
        return new Promise(function(resolve, reject) {
            UserService.findOne(params).then(function(userService) {
                if (userService != undefined) {
                    return resolve(userService);
                } else {
                    return reject(false);
                }
            });
        });
    },

    postUserService: function(body) {
        return new Promise(function(resolve, reject) {
            UserService.create(body).then(function(userService) {
                return resolve(userService);
            }).catch(function() {
                return reject(false);
            });
        });
    },

    putUserService: function(params, body) {
        return new Promise(function(resolve, reject) {
            UserService.findOneAndUpdate(params, body, {
                new: true
            }).then(function(userService) {
                if (userService != undefined) {
                    return resolve(userService);
                } else {
                    return reject(false);
                }
            });
        });
    },

    deleteUserService: function(params) {
        return new Promise(function(resolve, reject) {
            UserService.findOne(params).then(function(userService) {
                if (userService != undefined) {
                    UserService.remove({
                        _id: userService._id
                    }).then(function() {
                        return resolve(userService);
                    });
                } else {
                    return reject(false);
                }
            }).catch(function() {
                return reject(false);
            });
        });
    },

    regexJson: {
        "search": /\buser\b|\bservice\b/g
    }
}