const Promise = require('bluebird');
const fs = require('fs');
const pdf = require('dynamic-html-pdf');
module.exports = {
    bill: function() {
        return new Promise(function(resolve, reject) {
            const html = fs.readFileSync('./public/html/frontal.html', 'utf8');
            var options = {
                format: "A2",
                orientation: "portrait",
                border: "10mm"
            };

            var context = {
                username: "Jorge"
            }
            var document = {
                type: 'file',
                template: html,
                context: context,
                path: "./public/pdf/frontal.pdf"
            };
            pdf.create(document, options)
                .then(res => {
                    console.log(res)
                })
                .catch(error => {
                    console.error(error)
                });
        });
    }
}