const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Promise = require('bluebird')
const UserService = require('.././models/userService');
const helper = require('./helpers/index');
const helperUserService = require('./helpers/userService');
const validator = require('validator');

router.get('/userService', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helper.getQuery(req.query, helperUserService.regexJson.search).then(function(query) {
            helperUserService.getUserService(query).then(function(userService) {
                dataServer.data = userService;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.get('/userService/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperUserService.getUserServiceById(req.params).then(function(userService) {
                dataServer.data = userService;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.post('/userService', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        helperUserService.postUserService(req.body).then(function(userService) {
            dataServer.data = userService;
            helper.response(dataServer).then(function(data) {
                return res.send(data);
            });
        }).catch(function(errorString) {
            dataServer.data = errorString;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        });
    });
});

router.put('/userService/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperUserService.putUserService(req.params, req.body).then(function(userService) {
                dataServer.data = userService;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

router.delete('/userService/:_id', (req, res) => {
    helper.getDataServer(req).then(function(dataServer) {
        if (validator.isMongoId(req.params._id)) {
            helperUserService.deleteUserService(req.params).then(function(userService) {
                dataServer.data = userService;
                helper.response(dataServer).then(function(data) {
                    return res.send(data);
                });
            }).catch(function(errorString) {
                dataServer.data = errorString;
                helper.response(dataServer).catch(function(err) {
                    return res.send(err);
                });
            });
        } else {
            dataServer.data = false;
            helper.response(dataServer).catch(function(err) {
                return res.send(err);
            });
        }
    });
});

module.exports = router;