const gulp = require('gulp'),
    sass = require('gulp-sass');

gulp.task('sass', () =>
    gulp.src('./public/sass/*.sass')
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: true
        }))
        .pipe(gulp.dest('./public/css'))
);

// gulp.task('scss', () =>
//     gulp.src('./public/sass/*.scss')
//         .pipe(sass({
//             outputStyle: 'expanded',
//             sourceComments: true
//         }))
//         .pipe(gulp.dest('./public/css'))
// );

gulp.task('default', () => {
    gulp.watch('./public/sass/*.sass', ['sass'])
    // gulp.watch('./public/sass/*.scss', ['scss'])
});