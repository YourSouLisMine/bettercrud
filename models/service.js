let mongoose = require('mongoose');

let serviceSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        validate: /^[a-zA-Z][a-zA-Z\s]+$/
    },
    cost: {
        type: Number,
        required: true
    }
});

let Service = mongoose.model('Service', serviceSchema);

module.exports = Service;