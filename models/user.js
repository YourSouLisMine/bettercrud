let mongoose = require('mongoose');

let userSchema = mongoose.Schema({
    nickname: {
        type: String,
        required: true,
        unique: true,
        validate: /^[a-zA-Z0-9][\w-]{2,15}$/
    },
    name: {
        type: String,
        required: true,
        validate: /^[a-zA-Z][a-zA-Z\s]+$/
    },
    email: {
        type: String,
        required: true,
        validate: /^[a-zA-Z0-9][\w\.-]*@[\w\.-]+\.[a-z]{2,6}$/
    },
    password: {
        type: String,
        required: true,
        validate: /^[a-zA-Z0-9\.\/$]{60}$/ //md5
    },
    photo: {
        type: Array
    },
    info: {
        phone: {
            type: String,
            required: true,
            validate: /^[0-9]{10}$/
        },
        street: {
            type: String,
            required: true,
            validate: /^[a-zA-Z][a-zA-Z\s]+$/
        },
        home_number: {
            type: String,
            required: true,
            validate: /^[0-9]{1,4}$/
        },
        interior: {
            type: String,
            validate: /^[0-9]{1,4}$/
        },
        colony: {
            type: String,
            required: true,
            validate: /^[a-zA-Z][a-zA-Z\s]+$/
        },
        postal_code: {
            type: String,
            required: true,
            validate: /^[0-9]{5}$/
        },
        town: {
            type: String,
            required: true,
            validate: /^[a-zA-Z][a-zA-Z\s]+$/
        },
        city: {
            type: String,
            required: true,
            validate: /^[a-zA-Z][a-zA-Z\s]+$/
        }
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

let User = mongoose.model('User', userSchema);

module.exports = User;