let mongoose = require('mongoose');

let userServiceSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    service: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Service'
    }
});

let UserService = mongoose.model('user_service', userServiceSchema);

module.exports = UserService;